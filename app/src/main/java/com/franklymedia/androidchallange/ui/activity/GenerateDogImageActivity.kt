package com.franklymedia.androidchallange.ui.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.franklymedia.androidchallange.R
import com.franklymedia.androidchallange.data.preferences.PreferenceProvider
import com.franklymedia.androidchallange.databinding.ActivityGenerateDogImageBinding
import com.franklymedia.androidchallange.ui.viewmodel.GeneratedDogImageViewModel
import com.franklymedia.androidchallange.ui.viewmodelfactory.GenerateDogImageModelFactory
import com.franklymedia.androidchallange.utils.Coroutines
import com.franklymedia.androidchallange.utils.showToast
import com.franklymedia.androidchallange.utils.updateList
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class GenerateDogImageActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: GenerateDogImageModelFactory by instance()
    private lateinit var generateDogImageModel: GeneratedDogImageViewModel
    private val preferenceProvider: PreferenceProvider by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityGenerateDogImageBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_generate_dog_image)

        generateDogImageModel =
            ViewModelProvider(this, factory).get(GeneratedDogImageViewModel::class.java)

        binding.viewModel = generateDogImageModel
        setToolBar(resources.getString(R.string.generate_dog))

        generateDogImageModel.onSuccess.observe(this, Observer {
            Coroutines.main {
                if (it.status.toLowerCase(Locale.ENGLISH) == "success") {
                    updateList(it.message, preferenceProvider)
                    binding.data = it
                } else {
                    showToast("Something went wrong!")
                }
            }
        })

        generateDogImageModel.onFailure.observe(this, Observer { it ->
            Coroutines.main {
                showToast(it)
            }
        })
    }
}
