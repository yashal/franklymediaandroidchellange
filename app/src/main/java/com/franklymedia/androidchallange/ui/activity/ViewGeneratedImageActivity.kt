package com.franklymedia.androidchallange.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.franklymedia.androidchallange.R
import com.franklymedia.androidchallange.databinding.ActivityViewGeneratedImageBinding
import com.franklymedia.androidchallange.ui.viewmodel.ViewGeneratedImageViewModel
import com.franklymedia.androidchallange.ui.viewmodelfactory.ViewGeneratedImageModelFactory
import com.franklymedia.androidchallange.utils.BindingUtil
import com.franklymedia.androidchallange.utils.displayList
import kotlinx.android.synthetic.main.activity_view_generated_image.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ViewGeneratedImageActivity : BaseActivity() , KodeinAware {

    override val kodein by kodein()
    private val factory: ViewGeneratedImageModelFactory by instance()
    private lateinit var viewModel: ViewGeneratedImageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityViewGeneratedImageBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_view_generated_image)

        viewModel =
            ViewModelProvider(this, factory).get(ViewGeneratedImageViewModel::class.java)
        binding.viewModel = viewModel

        setToolBar(resources.getString(R.string.recently_generated))

        viewModel.clearDogImages.observe(this, Observer { it ->
            linearLayout.removeAllViews()
        })
        showCachedData()
    }

    private fun showCachedData(){
        val dogList = displayList()
        for(i in dogList.size-1 downTo 0) {
            val imageViewLayout: View = layoutInflater.inflate(R.layout.layout_image_view, null)
            val imageView: ImageView = imageViewLayout.findViewById(R.id.image)
            BindingUtil.setImage(imageView, dogList[i], null)
            linearLayout.addView(imageViewLayout)
        }
    }
}
