package com.franklymedia.androidchallange.ui.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.franklymedia.androidchallange.ui.viewmodel.HomeViewModel

class HomeViewModelFactory(
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel() as T
    }
}