package com.franklymedia.androidchallange.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel: ViewModel() {
    var buttonClicked = MutableLiveData<String>()

    fun onGenerateDogButtonClick() {
        buttonClicked.postValue(GENERATE_DOG)
    }

    fun onMyGenerateDogsButtonClick() {
        buttonClicked.postValue(MY_GENERATED_DOGS)
    }

    companion object {
        const val GENERATE_DOG = "GenerateDog"
        const val MY_GENERATED_DOGS = "MyGeneratedDogs"
    }
}