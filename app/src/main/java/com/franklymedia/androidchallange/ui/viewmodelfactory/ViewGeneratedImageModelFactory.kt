package com.franklymedia.androidchallange.ui.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.franklymedia.androidchallange.data.preferences.PreferenceProvider
import com.franklymedia.androidchallange.ui.viewmodel.ViewGeneratedImageViewModel

class ViewGeneratedImageModelFactory(
    private val preferenceProvider: PreferenceProvider
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ViewGeneratedImageViewModel(preferenceProvider) as T
    }
}