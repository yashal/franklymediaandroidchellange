package com.franklymedia.androidchallange.ui.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.franklymedia.androidchallange.data.repository.GenerateDogRepository
import com.franklymedia.androidchallange.ui.viewmodel.GeneratedDogImageViewModel

class GenerateDogImageModelFactory(
    private val repository: GenerateDogRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GeneratedDogImageViewModel(repository) as T
    }
}