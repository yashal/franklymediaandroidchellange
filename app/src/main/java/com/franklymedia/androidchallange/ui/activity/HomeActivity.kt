package com.franklymedia.androidchallange.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.franklymedia.androidchallange.R
import com.franklymedia.androidchallange.data.preferences.PreferenceProvider
import com.franklymedia.androidchallange.databinding.ActivityHomeBinding
import com.franklymedia.androidchallange.ui.viewmodel.HomeViewModel
import com.franklymedia.androidchallange.ui.viewmodel.HomeViewModel.Companion.GENERATE_DOG
import com.franklymedia.androidchallange.ui.viewmodel.HomeViewModel.Companion.MY_GENERATED_DOGS
import com.franklymedia.androidchallange.ui.viewmodelfactory.HomeViewModelFactory
import com.franklymedia.androidchallange.utils.initializeList
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class HomeActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: HomeViewModelFactory by instance()
    private val preferenceProvider: PreferenceProvider by instance()
    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityHomeBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_home)

        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        binding.viewModel = viewModel

        initializeList(preferenceProvider)

        viewModel.buttonClicked.observe(this, Observer { it ->
            if (!it.isNullOrEmpty()) {
                when (it) {
                    GENERATE_DOG -> {
                        Intent(this, GenerateDogImageActivity::class.java).also {
                            startActivity(it)
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        }
                    }
                    MY_GENERATED_DOGS -> {
                        Intent(this, ViewGeneratedImageActivity::class.java).also {
                            startActivity(it)
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        }
                    }
                }
            }
        })
    }
}
