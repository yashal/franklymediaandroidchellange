package com.franklymedia.androidchallange.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.franklymedia.androidchallange.data.network.response.DogBreedResult
import com.franklymedia.androidchallange.data.repository.GenerateDogRepository
import com.franklymedia.androidchallange.utils.ApiException
import com.franklymedia.androidchallange.utils.NoInternetException
import kotlinx.coroutines.launch

class GeneratedDogImageViewModel(
    private val repository: GenerateDogRepository
) : ViewModel() {
    val onSuccess = MutableLiveData<DogBreedResult>()
    val onFailure = MutableLiveData<String>()

    fun onGenerateClick() {
        viewModelScope.launch {
            try {
                val response = repository.generateDogImages()
                response.let {
                    val result = it
                    onSuccess.postValue(result)
                    return@launch
                }
            } catch (ex: ApiException) {
                ex.printStackTrace()
                onFailure.postValue(ex.message.toString())
            } catch (ex: NoInternetException) {
                ex.printStackTrace()
                onFailure.postValue(ex.message.toString())
            }
        }
    }

}