package com.franklymedia.androidchallange.ui.activity

import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.franklymedia.androidchallange.R


open class BaseActivity : AppCompatActivity() {

    var toolbar: Toolbar? = null
    var title: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun setToolBar(titleText: String) {
        toolbar = findViewById(R.id.toolbar)
        title = findViewById(R.id.title)
        title?.maxLines = 1
        title?.ellipsize = TextUtils.TruncateAt.END

        title?.text = titleText
        toolbar?.contentInsetStartWithNavigation = 0
        setSupportActionBar(toolbar)
        toolbar?.setNavigationIcon(R.drawable.ic_chevron_left)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

}