package com.franklymedia.androidchallange.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.franklymedia.androidchallange.data.preferences.PreferenceProvider
import com.franklymedia.androidchallange.utils.clearList

class ViewGeneratedImageViewModel(
    private val preferenceProvider: PreferenceProvider
) : ViewModel() {

    val clearDogImages = MutableLiveData<String>()

    fun clearDogClick() {
        clearList(preferenceProvider)
        clearDogImages.value = CLEAR
    }

    companion object {
        const val CLEAR = "Clear"
    }
}