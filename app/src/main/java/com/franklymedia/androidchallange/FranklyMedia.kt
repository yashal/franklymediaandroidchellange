package com.franklymedia.androidchallange

import android.app.Application
import com.franklymedia.androidchallange.data.network.NetworkConnectionInterceptor
import com.franklymedia.androidchallange.data.network.RestClientApi
import com.franklymedia.androidchallange.data.preferences.PreferenceProvider
import com.franklymedia.androidchallange.data.repository.GenerateDogRepository
import com.franklymedia.androidchallange.ui.viewmodelfactory.GenerateDogImageModelFactory
import com.franklymedia.androidchallange.ui.viewmodelfactory.HomeViewModelFactory
import com.franklymedia.androidchallange.ui.viewmodelfactory.ViewGeneratedImageModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class FranklyMedia: Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@FranklyMedia))
        bind() from singleton { RestClientApi(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { GenerateDogRepository(instance()) }
        bind() from provider { HomeViewModelFactory() }
        bind() from provider { GenerateDogImageModelFactory(instance()) }
        bind() from provider { ViewGeneratedImageModelFactory(instance()) }
    }
}