package com.franklymedia.androidchallange.data.network

import com.franklymedia.androidchallange.data.network.response.DogBreedResult
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RestClientApi {

    @GET("breeds/image/random")
    suspend fun generateDogImages(): Response<DogBreedResult>

    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): RestClientApi {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://dog.ceo/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(RestClientApi::class.java)
        }
    }
}