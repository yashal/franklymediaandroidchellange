package com.franklymedia.androidchallange.data.preferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.collections.ArrayList
private const val PREF_NAME = "FranklyMedia"
private const val IMAGE_OBJ = "DogImageObject"
class PreferenceProvider(var context: Context) {
    private var PRIVATE_MODE = 0
    private val preference: SharedPreferences
        get() =
            context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)

    fun saveImagesJsonObject(imagesObj: ArrayList<String?>) {
        val prefsEditor = preference.edit()
        val gson = Gson()
        val json = gson.toJson(imagesObj)
        prefsEditor.putString(IMAGE_OBJ, json)
        prefsEditor.apply()
    }

    fun getImagesJsonObject(): ArrayList<String?>? {
        val gson = Gson()
        val json = preference.getString(IMAGE_OBJ, "")
        val type: Type =
            object : TypeToken<List<String?>?>() {}.type
        return gson.fromJson(json, type)
    }
}