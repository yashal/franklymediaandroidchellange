package com.franklymedia.androidchallange.data.network.response

data class DogBreedResult(
    val message: String,
    val status: String
)