package com.franklymedia.androidchallange.data.repository

import com.franklymedia.androidchallange.data.network.RestClientApi
import com.franklymedia.androidchallange.data.network.SafeApiRequest
import com.franklymedia.androidchallange.data.network.response.DogBreedResult

class GenerateDogRepository(private val api: RestClientApi) : SafeApiRequest() {
    suspend fun generateDogImages(): DogBreedResult {
        return apiRequest { api.generateDogImages() }
    }
}