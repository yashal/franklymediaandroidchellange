package com.franklymedia.androidchallange

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.franklymedia.androidchallange.ui.activity.GenerateDogImageActivity
import com.franklymedia.androidchallange.ui.viewmodel.GeneratedDogImageViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class GenerateDogImageActivityTest {

    @Mock
    private lateinit var generatedDogImageViewModel: GeneratedDogImageViewModel


    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var generateDogImageActivity: GenerateDogImageActivity

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        generateDogImageActivity = GenerateDogImageActivity()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }
}