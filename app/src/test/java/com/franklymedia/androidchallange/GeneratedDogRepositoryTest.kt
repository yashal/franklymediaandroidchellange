package com.franklymedia.androidchallange

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.franklymedia.androidchallange.data.network.RestClientApi
import com.franklymedia.androidchallange.data.network.SafeApiRequest
import com.franklymedia.androidchallange.data.network.response.DogBreedResult
import com.franklymedia.androidchallange.data.preferences.PreferenceProvider
import com.franklymedia.androidchallange.data.repository.GenerateDogRepository
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class GeneratedDogRepositoryTest {

    @Mock
    private lateinit var myAPi: RestClientApi

    @Mock
    private lateinit var safeApiRequest: SafeApiRequest

    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var generateDogRepository: GenerateDogRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        generateDogRepository = GenerateDogRepository(myAPi)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }
    @Test
    fun `main repo executes api call`(){
        testScope.runBlockingTest {

            //Given
            val dogBreedResult = Mockito.mock(DogBreedResult::class.java)
            whenever( safeApiRequest.apiRequest { myAPi.generateDogImages()}).thenReturn(dogBreedResult)

            //When
            myAPi.generateDogImages()

            //Then
            verify(myAPi, times(1)).generateDogImages()
            verifyNoMoreInteractions(myAPi)
        }
    }
}

