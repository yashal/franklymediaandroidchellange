package com.franklymedia.androidchallange

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.franklymedia.androidchallange.data.network.response.DogBreedResult
import com.franklymedia.androidchallange.data.preferences.PreferenceProvider
import com.franklymedia.androidchallange.data.repository.GenerateDogRepository
import com.franklymedia.androidchallange.ui.viewmodel.GeneratedDogImageViewModel
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import kotlin.collections.ArrayList

class GeneratedDogViewModelTest {

    // Mock the repository object and verify interactions on this mock
    @Mock
    private lateinit var generateDogRepository: GenerateDogRepository

    @Mock
    private lateinit var preference: PreferenceProvider

    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var viewModel: GeneratedDogImageViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        viewModel = GeneratedDogImageViewModel(generateDogRepository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `main view model executes apiCall`() {

        testScope.runBlockingTest {

            val dogBreedResult = Mockito.mock(DogBreedResult::class.java)
            whenever(generateDogRepository.generateDogImages()).thenReturn(dogBreedResult)

            verify(generateDogRepository, times(0)).generateDogImages()
            verifyNoMoreInteractions(generateDogRepository)
        }
    }
}